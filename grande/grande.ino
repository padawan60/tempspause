//ce programme affiche le décompte dune grande pause de 1min 5sec

#include <TFT.h>
#define cs 10
#define dc 9
#define rst 8
unsigned long temps0;
unsigned long temps;
int tempsmin;
int tempssec;
int tempsint;
char secstr[3];
char minstr[3];
TFT TFTscreen = TFT(cs, dc, rst);

void setup() {
 TFTscreen.begin();
 TFTscreen.background(0,0,0);
 TFTscreen.stroke(255, 255, 255);

}

void loop() {
  //début boucle générale
  temps0= millis();
  //test si millis est démarré
  if (temps0 > 50){
    TFTscreen.background(0, 0, 255);
//début décompte
    while (millis()-temps0 < 65000){
      temps = 65000 - millis() + temps0;
// conversion en min : sec      
      tempsint = int(temps/1000);
      tempsmin = int(tempsint/60);
      tempssec = int(tempsint%60);     
// conversion en caractère       
      String sec = String(tempssec);
      sec.toCharArray(secstr,3);
      String minu = String(tempsmin);
      minu.toCharArray(minstr, 3);
//affichage
      TFTscreen.stroke(255, 0, 0);
      TFTscreen.setTextSize(2);
      TFTscreen.text("GRANDE", 10, 10);
      TFTscreen.setTextSize(2);
      TFTscreen.text("reste", 30, 40);
      TFTscreen.setTextSize(4);
      TFTscreen.text(minstr, 30 ,60);//affichage minute 
      TFTscreen.setTextSize(4);
      TFTscreen.text(":",55, 60);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);//affichage seconde
      
      delay(50);
// effacement min sec      
      TFTscreen.stroke(0, 0, 255);
      TFTscreen.setTextSize(4);
      TFTscreen.text(minstr, 30 ,60);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);
      
//fin boucle décompte      
    }
// fin boucle if    
  }
  
}
