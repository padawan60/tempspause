//ce programme affiche le décompte d'une petite pause de 10 sec


#include <TFT.h>
#define cs 10
#define dc 9
#define rst 8
unsigned long temps0;
unsigned long temps;
int tempssec;
int tempsint;
char secstr[3];

TFT TFTscreen = TFT(cs, dc, rst);

void setup() {
 TFTscreen.begin();
 TFTscreen.background(0,0,0);
 TFTscreen.stroke(255, 255, 255);
}
void loop() {
  //début boucle générale
  temps0= millis();
  //test si millis est démarré
  if (temps0 > 50){
    TFTscreen.background(0, 255, 0);
//début décompte
    while (millis()-temps0 < 10000){
      temps = 10000 - millis() + temps0; 
      tempsint = int(temps/1000);    
// conversion en caractère       
      String sec = String(tempsint);
      sec.toCharArray(secstr,3);
//affichage
      TFTscreen.stroke(0, 0, 255);
      TFTscreen.setTextSize(2);
      TFTscreen.text("PETITE", 10, 10);
      TFTscreen.setTextSize(2);
      TFTscreen.text("reste", 30, 40);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);//affichage seconde
      
      delay(50);
// effacement min sec      
      TFTscreen.stroke(0, 255, 0);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);
      
//fin boucle décompte      
    }
// fin boucle if    
  }
  
}
