//ce programme affiche une petite pause de 10 sec toutes les 20 min et une grande
//pause de 5 min après 2 petites pauses avec un buzzer


#include <TFT.h>
#define cs 10
#define dc 9
#define rst 8
unsigned long temps0;
unsigned long temps;
int tempsmin;
int tempssec;
int tempsint;
char secstr[3];
char minstr[3];
unsigned long duree;
char mess[15];
int choix;
int i;
int fondr;
int fondv;
int fondb;
int lettre[3];
TFT TFTscreen = TFT(cs, dc, rst);

void setup() {
  Serial.begin(9600);
  pinMode(2, OUTPUT); // initialise la connection buzzer
  TFTscreen.begin();
  TFTscreen.background(0,0,0);
  TFTscreen.stroke(255, 255, 255);

}

void loop() {
  buzzer();
  affminuterie(1);
  buzzer();
  affminuterie(2); 
  
  
  
  
  
  
  
  
  
}
// fonction du buzzer
void buzzer(){
 for (i=0; i<40; i++)
 {
  digitalWrite(2, HIGH);
  delay(2);
  digitalWrite(2, LOW);
  delay(2);
 }    
}

// fonction affichage 
int affminuterie(int choix){
  int fonde;
  //début boucle générale
  temps0= millis();
// définition paramètres en fonction du choix  
     if (choix == 1) {
     //      char mess[] = "grande";
           duree = 65000;
           fondr = 0;
           fondv = 0;
           fondb = 255;
       }
   else  if(choix == 2) {
      //    char mess[] = "petite";
           duree = 10000;
           fondr = 255;
           fondv = 0;
           fondb = 0;
           
           TFTscreen.background(255, 0, 0);
           TFTscreen.stroke(128, 128, 0);
   }
   else{
      //     char mess[] = "decompte";
           duree = 1200000;
    //       TFTscreen.background(0, 255, 0);
    //       TFTscreen.stroke(0, 128, 128);
    //       int fond[3] = {0, 255, 0};
    //       int lettre[] = {0, 128, 128};
           Serial.println("choix3");
    
  }
  //test si millis est démarré
  if (temps0 > 50){
    TFTscreen.background(fondr, fondv, fondb);
//début décompte
    while (millis()-temps0 < duree){
      temps = duree - millis() + temps0;
// conversion en min : sec      
      tempsint = int(temps/1000);
      tempsmin = int(tempsint/60);
      tempssec = int(tempsint%60);     
// conversion en caractère       
      String sec = String(tempssec);
      sec.toCharArray(secstr,3);
      String minu = String(tempsmin);
      minu.toCharArray(minstr, 3);
//affichage 
      char mess[]= "pause";
      TFTscreen.stroke(0, 0, 0);
      TFTscreen.setTextSize(2);
      TFTscreen.text(mess, 10, 10);
      TFTscreen.setTextSize(2);
      TFTscreen.text("reste", 30, 40);
      TFTscreen.setTextSize(4);
      TFTscreen.text(minstr, 30 ,60);//affichage minute 
      TFTscreen.setTextSize(4);
      TFTscreen.text(":",60, 60);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);//affichage seconde
      
      delay(50);
// effacement min sec      
      TFTscreen.stroke(fondr, fondv, fondb);
      TFTscreen.setTextSize(4);
      TFTscreen.text(minstr, 30 ,60);
      TFTscreen.setTextSize(4);
      TFTscreen.text(secstr, 80, 60);
      
//fin boucle décompte      
    }
// fin boucle if    
  }
  
}
